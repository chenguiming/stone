package com.ak.common.annotation;

import java.lang.annotation.*;

/**
 * 自定义多数据源切换注解
 *
 * @author vean
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {

    /**
     * 子系统编码
     */
    public String appCode();

    /**
     * 数据源类型
     * @return
     */
    public String databaseFlag() default "0";
}
